

all: 
	  echo COMPILE  
	  cd src  ; make 
	  ls   src/build/release/mupdf* 

clean: 
	  echo COMPILE  
	  cd src  ; make clean 
	  rm  src/build/release/mupdf-x11 

run: 
	  echo COMPILE  
	  cd src  ; make 

install:
	  echo INSTALL 
	  cp  src/build/release/mupdf-x11 /usr/local/bin/xpdfedit

mvinstall:
	  echo INSTALL 
	  mv  src/build/release/mupdf-x11 /usr/local/bin/xpdfedit

cpinstall:
	  echo INSTALL 
	  cp  src/build/release/mupdf-x11 /usr/local/bin/xpdfedit

lib:
	  apt-get update  ; apt-get install -y libx11-dev  git  libxi-dev libxrandr-dev  gcc clang g++ make 



ed:
	  echo edit 
	   echo vim src/platform/x11/x11_main.c
	   vim src/platform/x11/x11_main.c
	   echo vim src/platform/x11/x11_main.c


less:
	   less src/platform/x11/x11_main.c

totmp:
	   cp src/platform/x11/x11_main.c  /tmp



